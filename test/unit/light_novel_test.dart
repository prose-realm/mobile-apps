import 'package:prose_realm/models/lightnovel.dart';
import 'package:prose_realm/models/lightnoveldetail.dart';
import 'package:test/test.dart';

void main() {
  test('test /lightnovel and /lightnovel/:id', () async {
    final lightNovels = await LightNovelModel.fetchAll();
    for (var lightNovel in lightNovels) {
      expect(lightNovel.title, hasLength(greaterThan(0)));

      final fetchedLightNovel = await LightNovelDetailModel.fetchById(lightNovel.id);
      expect(fetchedLightNovel.title, equals(lightNovel.title));
      expect(fetchedLightNovel.description, hasLength(greaterThan(0)));
    }
  });
}
