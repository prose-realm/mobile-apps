import 'dart:core';

class Endpoint {
  static const apiScheme = 'https';
  static const apiHost = 'api.ventustium.com';
  static const prefix = '/api/v0';

  static Uri uri(String path, {required Map<String, dynamic> queryParameters}) {
    final uri = Uri(
      scheme: apiScheme,
      host: apiHost,
      path: '$prefix$path',
      queryParameters: queryParameters,
    );
    return uri;
  }
}
