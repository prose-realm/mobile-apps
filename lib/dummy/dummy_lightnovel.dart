import '../models/lightnovel.dart';

mixin DummyLightNovel implements LightNovelModel {
  static final List<LightNovelModel> items = [
    LightNovelModel(
        id: 1,
        title: "Gakusen Toshi Asterisk",
        cover:
            "https://api.ventustium.com/assets/images/lightnovel/no_cover.jpg"),
    LightNovelModel(
        id: 2,
        title: "Date a Live",
        cover:
            "https://api.ventustium.com/assets/images/lightnovel/no_cover.jpg")
  ];

  static LightNovelModel fetchAny() {
    return DummyLightNovel.items[0];
  }

  static List<LightNovelModel> fetchAll() {
    return DummyLightNovel.items;
  }

  static LightNovelModel fetch(int index) {
    return DummyLightNovel.items[index];
  }
}
