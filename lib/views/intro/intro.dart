import 'package:flutter/material.dart';
import 'package:prose_realm/components/pr_flat_button.dart';
import 'package:prose_realm/views/login_options/login_options.dart';

class Intro extends StatelessWidget {
  static const String routeName = '/intro';

  const Intro({super.key});

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      PageView(
        children: [
          Container(
            color: Theme.of(context).scaffoldBackgroundColor,
            child: Image.asset('assets/images/Prose_Realm_Logo_Light.png'),
          ),
        ],
      ),
      Positioned(
          width: MediaQuery.of(context).size.width,
          height: 60,
          bottom: 50,
          child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: PRFlatButton(
                const Text('Get Started'),
                backgroundColor: Colors.white,
                textColor: Colors.black,
                onTap: () {
                  Navigator.pushNamed(context, LoginOptions.routeName);
                },
              )))
    ]);
  }
}
