import 'dart:async';

import 'package:flutter/material.dart';
import 'package:prose_realm/lightnovel_detail.dart';
import 'package:prose_realm/models/lightnovel.dart';

class LibraryPager extends StatefulWidget {
  static const String routeName = '/library';

  const LibraryPager({super.key});

  @override
  createState() => _LibraryListState();
}

class _LibraryListState extends State<LibraryPager> {
  List<LightNovelModel> lightNovel = [];
  bool loading = false;

  @override
  void initState() {
    super.initState();
    loadData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Library'),
        ),
        // appBar: PreferredSize(
        //   preferredSize: Size.zero,
        //   child: AppBar(),
        // ),
        body: RefreshIndicator(
          onRefresh: loadData,
          child: Column(
            children: [
              renderProgressBar(context),
              Expanded(
                child: _lightNovelListView(context),
              )
            ],
          ),
        ));
  }

  Future<void> loadData() async {
    setState(() {
      loading = true;
    });
    Timer(const Duration(milliseconds: 0), () async {
      final lightNovel = await LightNovelModel.fetchAll();
      setState(() {
        this.lightNovel = lightNovel;
        loading = false;
      });
    });
  }

  Widget _listViewItemBuilder(BuildContext context, int index) {
    final lightNovel = this.lightNovel[index];
    return ListTile(
      contentPadding: const EdgeInsets.all(10.0),
      leading: _itemThumbnail(lightNovel),
      title: _itemTitle(lightNovel),
      onTap: () => _navigationToLightNovelDetail(context, lightNovel.id),
    );
  }

  Widget _lightNovelListView(BuildContext context) {
    return ListView.builder(
      itemCount: lightNovel.length,
      itemBuilder: _listViewItemBuilder,
    );
  }

  void _navigationToLightNovelDetail(BuildContext context, String id) {
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => LightNovelDetail(
            id,
          ),
        ));
  }

  Widget _itemThumbnail(LightNovelModel lightNovel) {
    return Container(
      constraints: const BoxConstraints.tightFor(width: 100.0),
      child: Image.network(lightNovel.cover, fit: BoxFit.fitHeight),
    );
  }

  Widget _itemTitle(LightNovelModel lightNovel) {
    return Text(lightNovel.title);
  }

  Widget renderProgressBar(BuildContext context) {
    return (loading
        ? const LinearProgressIndicator(
            value: null,
            backgroundColor: Colors.white,
            valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),
          )
        : Container());
  }
}
