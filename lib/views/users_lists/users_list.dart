import 'package:flutter/material.dart';

class UserLists extends StatefulWidget {
  static const String routeName = '/lists';
  const UserLists({super.key});

  @override
  State<UserLists> createState() => _UserListsState();
}

class _UserListsState extends State<UserLists> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Lists"),
      ),
    );
  }
}
