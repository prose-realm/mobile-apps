import 'package:flutter/material.dart';
import 'package:prose_realm/views/library_pager/library_pager.dart';
import 'package:prose_realm/views/login_options/login_options.dart';
import 'package:prose_realm/views/users_lists/users_list.dart';

void main() => runApp(const MainView());

class MainView extends StatelessWidget {
  static const String routeName = '/';
  const MainView({super.key});

  @override
  Widget build(BuildContext context) {
    return const NavigationMenu();
  }
}

class NavigationMenu extends StatefulWidget {
  const NavigationMenu({super.key});

  @override
  State<NavigationMenu> createState() => NavigationMenuState();
}

class NavigationMenuState extends State<NavigationMenu> {
  int currentPageIndex = 0;

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return Scaffold(
      bottomNavigationBar: NavigationBar(
        onDestinationSelected: (int index) {
          setState(() {
            currentPageIndex = index;
          });
        },
        indicatorColor: Colors.amber,
        selectedIndex: currentPageIndex,
        destinations: const <Widget>[
          NavigationDestination(
            selectedIcon: Icon(Icons.collections_bookmark),
            icon: Icon(Icons.collections_bookmark_outlined),
            label: 'Library',
          ),
          NavigationDestination(
            selectedIcon: Icon(Icons.list_alt),
            icon: Icon(Icons.list_alt_outlined),
            label: 'Lists',
          ),
          NavigationDestination(
            // icon: Badge(
            //     // label: Text('2'),
            //     child: Icon(Icons.person)),
            selectedIcon: Icon(Icons.person),
            icon: Icon(Icons.person_outlined),
            label: 'Profile',
          ),
        ],
      ),
      body: <Widget>[
        /// Library
        const LibraryPager(),

        /// User List page
        const UserLists(),

        /// Profile Page
        const LoginOptions(),
      ][currentPageIndex],
    );
  }
}
