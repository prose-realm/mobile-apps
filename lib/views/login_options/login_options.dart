import 'dart:async';
// import 'dart:convert' show json;

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
// import 'package:http/http.dart' as http;
import 'package:prose_realm/components/pr_flat_button.dart';
import 'package:prose_realm/components/pr_image_button.dart';
import 'package:prose_realm/config/styles.dart';

const List<String> scopes = <String>[
  'email',
  // 'https://www.googleapis.com/auth/contacts.readonly',
];

GoogleSignIn _googleSignIn = GoogleSignIn(
  // Optional clientId
  // clientId: 'your-client_id.apps.googleusercontent.com',
  scopes: scopes,
);

class LoginOptions extends StatefulWidget {
  static const String routeName = '/intro/login_options';

  const LoginOptions({super.key});

  @override
  createState() => _LoginOptionsState();
}

class _LoginOptionsState extends State<LoginOptions> {
  GoogleSignInAccount? _currentUser;

  bool _isChecked = false;
  // bool _isAuthorized = false; // has granted permissions?
  // String _contactText = '';

  @override
  void initState() {
    super.initState();

    _googleSignIn.onCurrentUserChanged
        .listen((GoogleSignInAccount? account) async {
      // #docregion CanAccessScopes
      // In mobile, being authenticated means being authorized...
      bool isAuthorized = account != null;
      // However, on web...
      if (kIsWeb && account != null) {
        isAuthorized = await _googleSignIn.canAccessScopes(scopes);
      }
      // #enddocregion CanAccessScopes

      setState(() {
        _isChecked = true;
        _currentUser = account;
        // _isAuthorized = isAuthorized;
      });

      // Now that we know that the user can access the required scopes, the app
      // can call the REST API.
      if (isAuthorized) {
        // unawaited(_handleGetContact(account!));
      }
    });
    _checkAuthentication();
    _googleSignIn.signInSilently();
  }

  Future<void> _checkAuthentication() async {
    GoogleSignInAccount? user = _googleSignIn.currentUser;
    if (user != null) {
      bool isAuthorized = await _googleSignIn.isSignedIn();
      setState(() {
        _currentUser = user;
        // _isAuthorized = isAuthorized;
      });

      // Call _handleGetContact if authorized
      if (isAuthorized) {
        // unawaited(_handleGetContact(user));
      }
    }
    _isChecked = true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Profile'),
      ),
      body: ConstrainedBox(
        constraints: const BoxConstraints.expand(),
        child: _buildBody(),
      ),
    );
  }

  Widget _buildBody() {
    final GoogleSignInAccount? user = _currentUser;
    if (user != null) {
      // The user is Authenticated
      return Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          ListTile(
            leading: GoogleUserCircleAvatar(
              identity: user,
            ),
            title: Text(user.displayName ?? ''),
            subtitle: Text(user.email),
          ),
          const Text('Signed in successfully.'),
          // if (_isAuthorized) ...<Widget>[
          //   // The user has Authorized all required scopes
          //   Text(_contactText),
          //   ElevatedButton(
          //     child: const Text('REFRESH'),
          //     onPressed: () => _handleGetContact(user),
          //   ),
          // ],
          // if (!_isAuthorized) ...<Widget>[
          //   // The user has NOT Authorized all required scopes.
          //   // (Mobile users may never see this button!)
          //   const Text('Additional permissions needed to read your contacts.'),
          //   ElevatedButton(
          //     onPressed: _handleAuthorizeScopes,
          //     child: const Text('REQUEST PERMISSIONS'),
          //   ),
          // ],
          ElevatedButton(
            onPressed: _handleSignOut,
            child: const Text('SIGN OUT'),
          ),
        ],
      );
    } else if (_isChecked == true && user == null) {
      // The user is NOT Authenticated
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          const Padding(
            padding: EdgeInsets.symmetric(vertical: 10.0),
            child: Text('You are not currently signed in.'),
          ),
          // This method is used to separate mobile from web code with conditional exports.
          // See: src/sign_in_button.dart
          Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: PRFlatButton(
                const PRImageButton('assets/images/google-logo-9808.png',
                    'Continue with Google'),
                backgroundColor: Colors.white,
                textColor: Styles.primaryTextColor,
                onTap: () => _handleSignIn(),
              ))
        ],
      );
    } else {
      return const Center(
        child: CircularProgressIndicator(),
      );
    }
  }

/*   Future<void>? _handleGetContact(GoogleSignInAccount user) async {
    setState(() {
      _contactText = 'Loading contact info...';
    });
    final http.Response response = await http.get(
      Uri.parse('https://people.googleapis.com/v1/people/me/connections'
          '?requestMask.includeField=person.names'),
      headers: await user.authHeaders,
    );
    if (response.statusCode != 200) {
      setState(() {
        _contactText = 'People API gave a ${response.statusCode} '
            'response. Check logs for details.';
      });
      if (kDebugMode) {
        print('People API ${response.statusCode} response: ${response.body}');
      }
      return;
    }
    final Map<String, dynamic> data =
        json.decode(response.body) as Map<String, dynamic>;
    final String? namedContact = _pickFirstNamedContact(data);
    setState(() {
      if (namedContact != null) {
        _contactText = 'I see you know $namedContact!';
      } else {
        _contactText = 'No contacts to display.';
      }
    });
  }*/

/*   String? _pickFirstNamedContact(Map<String, dynamic> data) {
    final List<dynamic>? connections = data['connections'] as List<dynamic>?;
    final Map<String, dynamic>? contact = connections?.firstWhere(
      (dynamic contact) => (contact as Map<Object?, dynamic>)['names'] != null,
      orElse: () => null,
    ) as Map<String, dynamic>?;
    if (contact != null) {
      final List<dynamic> names = contact['names'] as List<dynamic>;
      final Map<String, dynamic>? name = names.firstWhere(
        (dynamic name) =>
            (name as Map<Object?, dynamic>)['displayName'] != null,
        orElse: () => null,
      ) as Map<String, dynamic>?;
      if (name != null) {
        return name['displayName'] as String?;
      }
    }
    return null;
  } */

  Future<void> _handleSignIn() async {
    try {
      await _googleSignIn.signIn();
    } catch (error) {
      if (kDebugMode) {
        print(error);
      }
    }
  }

  // Prompts the user to authorize `scopes`.
  //
  // This action is **required** in platforms that don't perform Authentication
  // and Authorization at the same time (like the web).
  //
  // On the web, this must be called from an user interaction (button click).
  // #docregion RequestScopes

  /* Future<void> _handleAuthorizeScopes() async {
    final bool isAuthorized = await _googleSignIn.requestScopes(scopes);
    // #enddocregion RequestScopes
    setState(() {
      _isAuthorized = isAuthorized;
    });
    // #docregion RequestScopes
    if (isAuthorized) {
      unawaited(_handleGetContact(_currentUser!));
    }
    // #enddocregion RequestScopes
  } */

  Future<void> _handleSignOut() => _googleSignIn.disconnect();
}
