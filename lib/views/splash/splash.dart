import 'package:flutter/material.dart';
// import 'package:prose_realm/main.dart';

class Splash extends StatefulWidget {
  static const String routeName = '/init';

  const Splash({super.key});

  @override
  State<Splash> createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  @override
  void initState() {
    super.initState();
    _redirect();
  }

  Future<void> _redirect() async {
    await Future.delayed(const Duration(seconds: 0));
    // final session = supabase.auth.currentSession;
    if (!mounted) return;

    // if (session != null) {
    //   Navigator.of(context).pushReplacementNamed('/library');
    // } else {
    //   Navigator.of(context).pushReplacementNamed('/intro');
    // }
  }

  @override
  Widget build(BuildContext context) {
    return const Placeholder();
  }
}
