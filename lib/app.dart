import 'package:flutter/material.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:prose_realm/views/library_pager/library_pager.dart';
import 'package:prose_realm/views/login_options/login_options.dart';
import 'package:prose_realm/views/main_view/main_view.dart';
// import 'package:prose_realm/views/splash/splash.dart';
import 'package:prose_realm/views/users_lists/users_list.dart';
// import 'views/intro/intro.dart';

void app() {
  WidgetsBinding widgetsBinding = WidgetsFlutterBinding.ensureInitialized();
  FlutterNativeSplash.preserve(widgetsBinding: widgetsBinding);
  runApp(const App());
}

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    FlutterNativeSplash.remove();
    return MaterialApp(
        title: 'Prose Realm',
        debugShowCheckedModeBanner: false,
        initialRoute: MainView.routeName,
        theme: ThemeData(
          useMaterial3: true,
        ),
        darkTheme: ThemeData.dark(),
        themeMode: ThemeMode.dark,
        routes: {
          MainView.routeName: (context) => const MainView(),
          // Splash.routeName: (context) => const Splash(),
          // Intro.routeName: (context) => const Intro(),
          LoginOptions.routeName: (context) => const LoginOptions(),
          LibraryPager.routeName: (context) => const LibraryPager(),
          UserLists.routeName: (context) => const UserLists(),
        });
  }
}
