import 'package:flutter/material.dart';
import '/config/styles.dart';

class PRFlatButton extends StatelessWidget {
  final Widget? child;
  final GestureTapCallback? onTap;
  final Color? textColor;
  final Color? backgroundColor;

  const PRFlatButton(this.child,
      {super.key, this.textColor, this.backgroundColor, this.onTap});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        style: ElevatedButton.styleFrom(
            foregroundColor: (textColor ?? Colors.white),
            backgroundColor: (backgroundColor ?? Styles.primaryHighlightColor),
            elevation: 1,
            textStyle: const TextStyle(
                fontFamily: Styles.primaryFontRegular,
                fontWeight: Styles.primaryFontWeightLight,
                fontSize: Styles.primaryButtonFontSize),
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(5.0)))),
        onPressed: onTap,
        child: child);
  }
}
