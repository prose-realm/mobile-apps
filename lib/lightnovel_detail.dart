import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:prose_realm/models/lightnoveldetail.dart';

class LightNovelDetail extends StatefulWidget {
  final String id;

  const LightNovelDetail(this.id, {super.key});

  @override
  createState() => _LightNovelDetailState();
}

class _LightNovelDetailState extends State<LightNovelDetail> {
  LightNovelDetailModel lightNovel = LightNovelDetailModel.blank();

  String description = "";

  // _LightNovelDetailState(this.id);
  @override
  void initState() {
    super.initState();
    loadData();
  }

  Future<void> loadData() async {
    final lightNovel = await LightNovelDetailModel.fetchById(widget.id);

    if (mounted) {
      setState(() {
        this.lightNovel = lightNovel;
        description = "Description";
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(lightNovel.title),
        ),
        body: RefreshIndicator(
          onRefresh: () => loadData(),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: _renderLightNovelDetail(lightNovel)),
        ));
  }

  List<Widget> _renderLightNovelDetail(LightNovelDetailModel lightNovel) {
    final result = <Widget>[];
    result.add(_headSection(context));
    result.add(_sectionText(lightNovel.description));
    return result;
  }

  Widget _headSection(BuildContext context) {
    return NotificationListener<OverscrollIndicatorNotification>(
        onNotification: (overscroll) {
          overscroll.disallowIndicator();
          return true;
        },
        child: SingleChildScrollView(
          physics: const AlwaysScrollableScrollPhysics(),
          child: IntrinsicHeight(
            // Wrap with IntrinsicHeight
            child: Column(
              crossAxisAlignment: CrossAxisAlignment
                  .stretch, // Ensure column stretches horizontally
              children: [
                _headTitle(context),
                _sectionTitle(description),
              ],
            ),
          ),
        ));
  }

  Widget _headTitle(BuildContext context) {
    return Column(children: [
      Row(
        children: [_coverImage(lightNovel.cover, 225.0)],
      ),
    ]);
  }

  Widget _sectionTitle(String text) {
    return Container(
      padding: const EdgeInsets.fromLTRB(25.0, 25.0, 25.0, 10.0),
      child: Text(
        text,
        textAlign: TextAlign.left,
        style: const TextStyle(
          fontSize: 20.0,
        ),
      ),
    );
  }

  Widget _sectionText(String text) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(25.0, 5.0, 25.0, 10.0),
        child: SingleChildScrollView(
          physics: const AlwaysScrollableScrollPhysics(),
          child: Text(
            text,
            softWrap: true,
          ),
        ),
      ),
    );
  }

  Widget _coverImage(String url, double height) {
    try {
      if (url.isNotEmpty) {
        return Container(
            constraints: BoxConstraints.tightFor(height: height),
            child: GestureDetector(
                onTap: () async {
                  await showDialog(
                      context: context, builder: (_) => _coverImagePopUp(url));
                },
                child: Padding(
                    padding: const EdgeInsets.fromLTRB(25.0, 5.0, 25.0, 0),
                    child: Image.network(url))));
      }
    } catch (e) {
      if (kDebugMode) {
        print("Could not load image $url");
      }
    }
    return Container();
  }

  Widget _coverImagePopUp(String url) {
    try {
      if (url.isNotEmpty) {
        return Dialog(
          child: Image.network(url),
        );
      }
    } catch (e) {
      if (kDebugMode) {
        print("Could not load image $url");
      }
    }
    return Container();
  }
}
