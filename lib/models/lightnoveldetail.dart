import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:prose_realm/endpoint.dart';
import 'package:http/http.dart' as http;

part 'lightnoveldetail.g.dart';

@JsonSerializable()
class LightNovelDetailModel {
  final String id;
  final String title;
  final String description;
  final String? status;
  final String cover;
  final int verif;

  LightNovelDetailModel({
    required this.id,
    required this.title,
    required this.description,
    required this.cover,
    required this.verif,
    required this.status,
  });

  LightNovelDetailModel.blank()
      : id = '',
        title = '',
        description = '',
        status = null,
        cover = '',
        verif = 0;

  factory LightNovelDetailModel.fromJson(Map<String, dynamic> json) =>
      _$LightNovelDetailModelFromJson(json);

  static Future<LightNovelDetailModel> fetchById(String id) async {
    var uri = Endpoint.uri('/lightnovel/series/$id', queryParameters: {});

    final resp = await http.get(uri);

    if (resp.statusCode != 200) {
      throw (resp.body);
    }
    final Map<String, dynamic> decodedResponse = json.decode(resp.body);
    return LightNovelDetailModel.fromJson(decodedResponse['data']);
  }
}
