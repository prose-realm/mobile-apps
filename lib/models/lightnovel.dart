import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:prose_realm/endpoint.dart';
import 'package:http/http.dart' as http;

part 'lightnovel.g.dart';

@JsonSerializable()
class LightNovelModel {
  final String id;
  final String title;
  final String cover;

  LightNovelModel(
      {required this.id,
      required this.title,
      required this.cover,
      });

  LightNovelModel.blank()
      : id = '',
        title = '',
        cover = '';

  factory LightNovelModel.fromJson(Map<String, dynamic> json) =>
      _$LightNovelModelFromJson(json);

  static Future<List<LightNovelModel>> fetchAll() async {
    var uri = Endpoint.uri('/lightnovel/series', queryParameters: {});

    final resp = await http.get(uri);

    if (resp.statusCode != 200) {
      throw (resp.body);
    }
    Map<String, dynamic> decodedResponse = json.decode(resp.body);
    List<dynamic> dataList = decodedResponse['data'];
    List<LightNovelModel> list = <LightNovelModel>[];
    for (var jsonItem in dataList) {
      list.add(LightNovelModel.fromJson(jsonItem));
    }
    return list;
  }
}
