// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'lightnoveldetail.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LightNovelDetailModel _$LightNovelDetailModelFromJson(
        Map<String, dynamic> json) =>
    LightNovelDetailModel(
      id: json['id'] as String,
      title: json['title'] as String,
      description: json['description'] as String,
      cover: json['cover'] as String,
      verif: json['verif'] as int,
      status: json['status'] as String?,
    );

Map<String, dynamic> _$LightNovelDetailModelToJson(
        LightNovelDetailModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'description': instance.description,
      'status': instance.status,
      'cover': instance.cover,
      'verif': instance.verif,
    };
