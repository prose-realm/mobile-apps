// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'lightnovel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LightNovelModel _$LightNovelModelFromJson(Map<String, dynamic> json) =>
    LightNovelModel(
      id: json['id'] as String,
      title: json['title'] as String,
      cover: json['cover'] as String,
    );

Map<String, dynamic> _$LightNovelModelToJson(LightNovelModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'cover': instance.cover,
    };
