# Prose Realm Mobile Apps

Light Novel Database

## Description

This is are a new version of My Light Novel List. This app will be using Flutter instead Android Studio.

## Dependencies
- Flutter Version 3.10.5
- flutter_native_splash
- change_app_package_name
- json_annotation: ^4.8.1
- http: ^1.1.0
- supabase_flutter: 

### Flutter Native Splash

```
dart run flutter_native_splash:create
```

### change_app_package_name
```
flutter pub run change_app_package_name:main com.ventustium.proserealm
```

### Make JSON Serialization
```
dart run build_runner build
```

### Build with
```
java version: 20
android version: 14
```